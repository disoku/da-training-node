import 'babel-polyfill';
import Router from 'koa-router';
import { baseApi } from '../config';
import UsersController from '../controllers/users';
import jwt from '../middlewares/jwt';

const api = 'users';

const router = new Router();

router.prefix(`/${baseApi}/${api}`);


// POST /api/users
// This route is protected, call POST /api/authenticate to get the token
router.post('/', jwt, UsersController.add);

export default router;
