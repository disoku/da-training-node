import routesLoader from '../utils/routesLoader';
import BaseController from '../controllers/base';

export default function(app) {
  routesLoader(`${__dirname}`).then(files => {
    files.forEach(route => {
        app
          .use(route.routes())
          .use(route.allowedMethods({throw: true})
      );
    });

    app.use(BaseController.home);
  });
}
