const webpack = require('webpack');
const path = require('path');



const BUILD_DIR = path.resolve(__dirname, 'client/public');
const APP_DIR = path.resolve(__dirname, 'client/app');

const config = {
  // entry:'C:/Users/dsolyanik/WebstormProjects/da-training.node/client/app/index',
  entry: APP_DIR + '/index.js',
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  },
  module : {
    loaders : [
      {
        test : /\.js?/,
        include : APP_DIR,
        loader : 'babel-loader'
      },
      {
        test: /\.less$/,
        loader: "css-loader!less-loader"
      }
    ]
  }
};

module.exports = config;