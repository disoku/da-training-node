import React, { Component } from 'react';
import { render } from 'react-dom';
import { Router, Route, browserHistory, Link } from 'react-router';
import Login from './components/base/Login'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

class Home extends Component {
  render(){
    return (<h1>
      Hi
      <li><Link to="/about">About</Link></li>
    </h1>);
  }
}

class Car extends Component {
  render(){
    return (<h1>Cars page</h1>);
  }
}

class About extends Component {
  render(){
    return (<h1>About page</h1>);
  }
}


const rootEl = document.getElementById('container');

render(
    <MuiThemeProvider muiTheme={getMuiTheme()}>
      <Router history={browserHistory} path="/">
        <Route path="/" component={Home}/>
        <Route path="/login" component={Login}/>
        <Route path="/cars" component={Car}/>
        <Route path="/about" component={About}/>
      </Router>
    </MuiThemeProvider>,
    rootEl,
);

// todo add Protecting Routes
// const requireAuth = (nextState, replace) => {
//   if (!auth.isAdmin()) {
//     // Redirect if not an Admin
//     replace({ pathname: '/' })
//   }
// }
// export const AdminRoutes = () => {
//   return (
//       <Route path="/admin" component={Admin} onEnter={requireAuth} />
//   )
// }