import React, { PureComponent } from 'react';
import {render} from 'react-dom';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import './login.less'


class Login extends PureComponent {
  render() {
    return (
        <span className="login-form">
          <form>
            <div>
              <TextField
                  hintText="enter your login"
                  floatingLabelText="Login"
              />
            </div>
            <div>
              <TextField
                  hintText="enter your password"
                  floatingLabelText="Password"
              />
            </div>
            <div>
              <Button type="submit">Sign in</Button>
            </div>
          </form>
        </span>
    );
  }
}

export default Login;
